<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Sign Up For New Student</title>

    <style>
        .signup-background {
            width: 20rem;
            border: solid 2px #4e7aa3;
            margin: auto;
            padding: 0.6rem 0.8rem;
        }

        .signup {
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            padding: 1.2rem 2rem;
        }

        .signup-form {
            font-size: 16px;
            width: 100%;
            display: flex;

        }

        .signup-form-text {
            background-color: #5b9bd5;
            border: 2px solid #4e7aa3;
            width: 8rem;
            padding: 0.6rem 0.6rem 0.2rem;
            margin-right: 2rem;
            text-align: center;
            color: #fff;
        }

        .input-text {
            width: 20rem;
            height: 2.6rem;
            padding-top: 2rem;
        }

        .input-text-img {
            margin-top: 0.8rem;
        }

        .signup-submit {
            display: flex;
            justify-content: center;
            margin-top: 2rem;
        }

        input[type="submit"] {
            height: 2.8rem;
            width: 8rem;
            background-color: #70ad46;
            border-radius: 10px;
            border: solid 2px #477990;
            cursor: pointer;
            color: white;
        }
    </style>
</head>

<body>
    <fieldset class="signup-background">
        <div class="signup">
            <?php
                session_start();
                $name = isset($_SESSION['name']) ? $_SESSION['name'] : "";
                $gender = isset($_SESSION['gender']) ? $_SESSION['gender'] : "";
                $major = isset($_SESSION['major']) ? $_SESSION['major'] : "";
                $birthday = isset($_SESSION['birthday']) ? $_SESSION['birthday'] : "";
                $address = isset($_SESSION['address']) ? $_SESSION['address'] : "";
                $image = isset($_SESSION['image']) ? $_SESSION['image'] : "";
            ?>
 
            <form method="POST" id="form" enctype="multipart/form-data">
                <div class="signup-form">
                    <p class="signup-form-text">
                        Họ và tên
                    </p>

                    <div class="input-text"> <?php echo $name?> </div>
                </div>
                <div class="signup-form">
                    <p class="signup-form-text">
                        Giới tính
                    </p>

                    <div class="input-text"> <?php echo $gender?> </div>

                </div>
                <div class="signup-form">
                    <p class="signup-form-text">
                        Phân khoa
                    </p>
                    <div class="input-text"> <?php echo $major?> </div>
                </div>
                <div class="signup-form">
                    <p class="signup-form-text">
                        Ngày sinh
                    </p>

                    <div class="input-text"> <?php echo $birthday?> </div>
                </div>
                <div class="signup-form">
                    <p class="signup-form-text">
                        Địa chỉ
                    </p>
                    <div class="input-text"> <?php echo $address?> </div>
                </div>


                <div class="signup-form">
                    <p class="signup-form-text">
                        Hình ảnh
                    </p>
                    <div class="input-text-img">
                        <?php
                            echo '<span ><img src="' . $image . '" height="60px" width="60px"></span>'
                        ?>
                    </div>
                </div>

                <div class="signup-form signup-submit">
                    <input type="submit" value="Xác nhận">
                </div>
            </form>
        </div>

    </fieldset>


</body>

</html>

